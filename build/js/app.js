'use strict';

document.addEventListener("DOMContentLoaded", function() {

	let ie_detect = (!(window.ActiveXObject) && "ActiveXObject" in window);

	if (ie_detect) {
		$('body').addClass('ie-fix');
	}
	else {
		$('body').addClass('non-ie');
	}

    // SVG IE11 support
    svg4everybody();
    
    // Mask input
    $("input[name='phone']").mask("+7(999) 999-9999");

    // Mobile Nav Toggle
    $('.nav-toggle').on('click', function(e){
        e.preventDefault();
        $('.nav').toggleClass('open');
    });

    // Modal 
    $('.btn-modal').fancybox({
        autoFocus: false,
    });

    $('.btn-scroll').on('click', function () {
        var elementClick = $(this).attr("href");
        var destination = $(elementClick).offset().top;
        $('html, body').animate({ scrollTop: destination }, 600);
        return false;
    });

    // home.clients.slider
    const clients = new Swiper('.clients-slider', {
        loop: true,
        slidesPerView: 1,
        spaceBetween: 0,
        navigation: {
            nextEl: '.clients__nav--next',
            prevEl: '.clients__nav--prev',
        },
        breakpoints: {
            576: {
                slidesPerView: 3,
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 0,
            },
            1024: {
                slidesPerView: 3,
                spaceBetween: 0,
            },
            1240: {
                slidesPerView: 3,
                spaceBetween: 40,
            },
        }
    });

    // comments.slider
    let isСomments  = $('div').is('.comments');
    if (isСomments) {
        const comments = new Swiper('.comments__slider', {
            loop: true,
            watchSlidesVisibility: true,
            watchSlidesProgress: true,
            loopAdditionalSlides: 0,
            slidesPerView: 1,
            spaceBetween: 30,
            navigation: {
                nextEl: '.comments__button--next',
                prevEl: '.comments__button--prev',
            }
        });
        comments.on('slideChange', function () {
            let sld = comments.realIndex + 1;
            $('.comments__nav--current').text(sld);
        });

    }

    // advantage.slider
    let isAdvantage  = $('div').is('.advantage');
    if (isAdvantage) {
        const advantage = new Swiper('.advantage-slider', {
            loop: true,
            slidesPerView: 1,
            spaceBetween: 0,
            navigation: {
                nextEl: '.advantage__nav--next',
                prevEl: '.advantage__nav--prev',
            }
        });
        advantage.on('slideChange', function () {
            let sld = advantage.realIndex + 1;
            $('.advantage__counter--current').text(sld);
        });
    }

    // services.slider
    const services = new Swiper('.services-slider', {
        loop: false,
        slidesPerView: 1,
        spaceBetween: 20,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        breakpoints: {
            768: {
                spaceBetween: 40,
            }
        }
    });

    services.on('slideChange', function () {
        $('.services__main').toggleClass('switch');
    });

    $('.services__item').on('click', function (e) {
        e.preventDefault();
        let idx = $(this).attr('data-index') - 1;
        console.log(idx);
        services.slideTo ( idx , 400 );
    });

    // fresh.slider
    const fresh = new Swiper('.fresh-slider', {
        loop: true,
        speed: 800,
        slidesPerView: 'auto',
        spaceBetween: 40,
        navigation: {
            nextEl: '.fresh__nav--next',
            prevEl: '.fresh__nav--prev',
        },
        breakpoints: {
            768: {
                spaceBetween: 40,
            },
            1240: {
                spaceBetween: 80,
                slidesPerGroup: 2
            },
            1340: {
                spaceBetween: 160,
                slidesPerGroup: 2
            }
        }
    });

    // tabs nav
    const tabs_nav = new Swiper('.tabs-nav', {
        direction: 'horizontal',
        slidesPerView: 'auto',
        spaceBetween: 30,
        freeMode: true,
        mousewheel: true,
    });

    $('.tabs__nav--item').on('click', function (e){
        e.preventDefault();
        let box = $($('.tabs'));
        let tab = $($(this).attr('data-target'));
        box.find('.tabs__nav--item').removeClass('active');
        $(this).addClass('active');

        box.find('.tabs__item').hide(500);
        box.find(tab).show(500);
    });

    let $projectsSlider = $('.projects-slider');
    $projectsSlider.owlCarousel({
        merge: true,
        margin: 20,
        items: 1,
        dots: false,
        loop: true,
        smartSpeed: 1100,
        navigation : false,
        responsiveClass:true
    });

    $('.projects__switch--prev').on('click', function(e) {
        e.preventDefault();
        $projectsSlider.trigger('prev.owl.carousel');
    })

    $('.projects__switch--next').on('click', function(e) {
        e.preventDefault();
        $projectsSlider.trigger('next.owl.carousel');
    })

    $('.projects__nav--button').on('click', function (e){
        e.preventDefault();
        let box = $($(this).closest('.projects'));
        let tab = $($(this).attr('data-target'));
        box.find('.projects__nav--button').removeClass('active');
        $(this).addClass('active');

        box.find('.projects__tab').removeClass('active');
        box.find(tab).addClass('active');

        $projectsSlider.trigger('refresh.owl.carousel');
    });


    $('.project-nav__elem').onmouseover = function(event) {
        $('.project-nav__elem').toggleClass('invert');
    };

    $( ".project-nav__elem" ).mouseenter(function() {
       $('.project-nav__elem').toggleClass('invert');
    })

    $( ".project-nav" ).mouseleave(function() {
   //     $('.project-nav__elem').toggleClass('invert');
    })

    // tags
    const tags = new Swiper('.tags-nav', {
        direction: 'horizontal',
        slidesPerView: 'auto',
        spaceBetween: 30,
        freeMode: true,
        mousewheel: true,
    });

    // post-switch
    let isPostSwitch  = $('div').is('.post-switch');
    if (isPostSwitch) {

        const post_switch = new Swiper('.post-switch-slider', {
            loop: true,
            speed: 800,
            slidesPerView: 1,
            spaceBetween: 20,
            navigation: {
                nextEl: '.post-switch-next',
                prevEl: '.post-switch-prev',
            },
            breakpoints: {
                576: {
                    spaceBetween: 20,
                    slidesPerView: 2,
                },
                768: {
                    spaceBetween: 40,
                    slidesPerView: 2,
                },
                1024: {
                    spaceBetween: 80,
                    slidesPerView: 2,
                },
                1320: {
                    spaceBetween: 160,
                    slidesPerView: 2,
                }
            }
        });
        /*
        post_switch.on('slideChange', function () {
            let sld = post_switch.realIndex + 1;
            $('.post-switch__nav--current').text(sld);
        });
        */
    }

    AOS.init({
        disable: 'mobile'
    });

    let sections = $('.section'),
        nav = $('.root-header')
        , nav_height = nav.outerHeight();

    $(window).on('scroll', function () {
        var cur_pos = $(this).scrollTop();

        sections.each(function() {
            var top = $(this).offset().top - (nav_height / 2),
                bottom = top + $(this).outerHeight();

            if (cur_pos >= top && cur_pos <= bottom) {
                sections.removeClass('active');
                $(this).addClass('active');

                nav.removeClass('header-black');
                nav.removeClass('header-white');
	            nav.removeClass('header-white-black');

                let navColor =  $(this).attr('data-color');
                nav.addClass(navColor);
            }
        });
    });

    $.fn.textInputBox = function (boxClass) {
        if (!boxClass) boxClass = 'input-field';
        $(this).find('.' + boxClass).each(function (index, element) {
            const $box = $(element);
            const $input = $box.find('input, textarea');
            if ($input.attr('required') && !$box.hasClass(`${boxClass}_required`)) {
                $box.addClass(`${boxClass}_required`);
            }
            if ($input.attr('placeholder')) {
                $box.attr('data-placeholder', $input.attr('placeholder'));
            }
            hasValue();
            $input.on('change', hasValue);
            $input.on('focus', function () {
                $box.addClass(`${boxClass}_focus`);
                hasValue();
            }).on('blur', function () {
                $box.removeClass(`${boxClass}_focus`);
                hasValue();
            });

            function hasValue() {
                const hasValueClass = `${boxClass}_has-value`;
                if ($input.val().length > 0) {
                    if (!$box.hasClass(hasValueClass)) $box.addClass(hasValueClass);
                } else {
                    if ($box.hasClass(hasValueClass)) $box.removeClass(hasValueClass);
                }
            }
        });
    }

    $('body').textInputBox();


    // comments.slider
    let isPrice  = $('div').is('.price');
    if (isPrice) {
        const priceSlider = new Swiper('.price-slider', {
            loop: true,
            watchSlidesVisibility: true,
            watchSlidesProgress: true,
            loopAdditionalSlides: 0,
            slidesPerView: 1,
            spaceBetween: 30,
            navigation: {
                nextEl: '.price__nav--next',
                prevEl: '.price__nav--prev',
            }
        });
        priceSlider.on('slideChange', function () {
            let sld = priceSlider.realIndex + 1;
            $('.price__nav--current').text(sld);
        });

    }


    // Price hovers
    $(function () {
        $('.price-item').mouseover(function(){
            $('.price__main').addClass('hovers');
            $('.price__main').find('.price-item').removeClass('hover');
            $(this).addClass('hover');
            console.log('hover');
        });

        $('.price-item').mouseout(function(){
            $('.price__main').removeClass('hovers');
            $(this).removeClass('hover');
        });
    });

    // Brief

    $('.brief-next').on('click', function(e) {
        e.preventDefault();
        let $step = $('.step-' + $(this).attr('data-step'));

        $(this).closest('.brief__step').removeClass('active');
        $step.addClass('active');
    })


    $('.brief-prev').on('click', function(e) {
        e.preventDefault();
        let $step = $('.step-' + $(this).attr('data-step'));
        $(this).closest('.brief__step').removeClass('active');
        $step.addClass('active');
    })
});




